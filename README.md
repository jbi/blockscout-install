# Blockscout install #

A vagrant/ansible implementation of install of the [Blockscout](https://github.com/poanetwork/blockscout) ethereum blockchain exlorer.

## Information ##

- This setup has **NOT** been setup for production use.  Permissions etc. have not been locked down in any way & has not been setup using best practices.  This is purely for trying to get blockscout up and running locally.

- The Vagrantfile is currently set to create a VM with 16Gb.  This is due to the Geth process getting killed due to low memory when built with lower memory. Excerpt from Vagrantfile:

```
config.vm.provider "virtualbox" do |v|
   v.memory = 16384
end
```

- There is a copy of the Geth blockchain data synced from `/mnt/usbhdd` to `/nfs/ethereum/` on the vagrant vm.

- Geth has been started with the command `geth --datadir /nfs/ethereum --ipcpath=/tmp/geth.ipc --syncmode "full" --rpc --rpcaddr 0.0.0.0 --port 30303 --rpcport 8545 --rpcapi debug,net,eth,shh,web3,txpool --wsa    pi "eth,net,web3,network,debug,txpool" --ws --wsaddr 0.0.0.0 --wsport 8546 --wsorigins "*" --gcmode=archive`

- As the blockchain data is stored on an external USB drive, the ipcpath has been changed to `/tmp/geth.ipc`

- To run the blockscout server, after getting ssh access to the vagrant box:

```
cd /opt/blockscout
sudo mix phx.server
```
